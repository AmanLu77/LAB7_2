#include <inc/matrix.hpp>

// �����������
Matrix::Matrix(int n, int m)
{
	//std::cout << "Constructor" << std::endl;
	m_n = n;
	m_m = m;
	m_mat = new float* [m_n];
	for (int i = 0; i < m_n; i++)
		m_mat[i] = new float[m_m];
}
// ����������� �����������
Matrix::Matrix(const Matrix& mat)
{
	m_n = mat.m_n;
	m_m = mat.m_m;

	m_mat = new float* [m_n];
	for (int i = 0; i < m_n; i++)
		m_mat[i] = new float[m_m];

	for (int i = 0; i < m_n; i++)
		for (int j = 0; j < m_m; j++)
			m_mat[i][j] = mat.m_mat[i][j];
}
// ������������
Matrix& Matrix::operator=(const Matrix& mat)
{
	m_n = mat.m_n;
	m_m = mat.m_m;

	for (int i = 0; i < m_n; i++)
		for (int j = 0; j < m_m; j++)
			m_mat[i][j] = mat.m_mat[i][j];

	return *this;
}
// �������� ��������
Matrix Matrix::operator+(const Matrix& mat)
{
	//std::cout << "operator+" << std::endl;
	int t = 1, r = 1;
	if (mat.m_m == m_m && mat.m_n == m_n)
	{
		t = mat.m_m;
		r = mat.m_n;
	}
	else
	{
		std::cout << "Error +" << std::endl;
		Matrix tmp1(0, 0);
		return tmp1;
	}
	Matrix tmp(r, t);
	for (int i = 0; i < m_n; i++)
		for (int j = 0; j < m_m; j++)
			tmp.m_mat[i][j] = m_mat[i][j] + mat.m_mat[i][j];
	return tmp;
}

// �������� ���������
Matrix Matrix::operator-(const Matrix& mat)
{
	//std::cout << "operator-" << std::endl;
	int t = 1, r = 1;
	if (mat.m_m == m_m && mat.m_n == m_n)
	{
		t = mat.m_m;
		r = mat.m_n;
	}
	else
	{
		std::cout << "Error -" << std::endl;
		Matrix tmp1(0, 0);
		return tmp1;
	}
	Matrix tmp(r, t);
	for (int i = 0; i < m_n; i++)
		for (int j = 0; j < m_m; j++)
			tmp.m_mat[i][j] = m_mat[i][j] - mat.m_mat[i][j];
	return tmp;
}

// �������� ���������
Matrix Matrix::operator*(const Matrix& mat)
{
	//std::cout << "operator*" << std::endl;
	int t = 1, r = 1;
	if (mat.m_n == m_m)
	{
		t = m_m;
		r = mat.m_n;
	}
	else
	{
		std::cout << "Error *" << std::endl;
		Matrix tmp1(0, 0);
		return tmp1;
	}

	Matrix tmp(r, t);
	for (int i = 0; i < m_m; i++)
	{
		for (int j = 0; j < mat.m_n; j++)
		{
			tmp.m_mat[i][j] = 0;
			for (int k = 0; k < m_n; k++)
			{
				tmp.m_mat[i][j] += m_mat[i][k] * mat.m_mat[k][j];
			}
		}
	}
	return tmp;
}

// ������������ 2�2 � 3�3
float Matrix::Det()
{
	float d = 0;
	if (m_n == 2 && m_m == 2)
	{
		d += m_mat[0][0] * m_mat[1][1];
		d -= m_mat[0][1] * m_mat[1][0];
		return d;
	}
	else if (m_n == 3 && m_m == 3)
	{
		d = m_mat[0][0] * m_mat[1][1] * m_mat[2][2] - m_mat[0][0] * m_mat[1][2] * m_mat[2][1]
			- m_mat[0][1] * m_mat[1][0] * m_mat[2][2] + m_mat[0][1] * m_mat[1][2] * m_mat[2][0]
			+ m_mat[0][2] * m_mat[1][0] * m_mat[2][1] - m_mat[0][2] * m_mat[1][1] * m_mat[2][0];
		return d;
	}
	else if (m_n == 1 && m_m == 1)
	{
		d = m_mat[0][0];
		return d;
	}
	else
	{
		std::cout << "�������� �� ��������������" << std::endl;
		return 0;
	}
}

// ����������
Matrix::~Matrix()
{
	//std::cout << "Destructor" << std::endl;
	for (int i = 0; i < m_n; i++)
		delete[] m_mat[i];
	delete m_mat;
}

int Matrix::Getn() { return m_n; }

int Matrix::Getm() { return m_m; }

// �����
Matrix Matrix::Minor(int a1, int a2)
{
	Matrix E(m_n - 1, m_m - 1);
	if (m_m != m_n)
	{
		std::cout << "�������� �� ��������������" << std::endl;
		return E;
	}
	else
	{
		int i1 = 0, j1 = 0;
		for (int i = 0; i < m_n; i++) {

			for (int j = 0; j < m_m; j++) {

				if (i != a1) {

					if (j != a2) {
						E.m_mat[i1][j1] = m_mat[i][j];
						j1++;

						if (j1 % (m_m - 1) == 0) {

							i1++;
							j1 = 0;

							if (i1 >= m_m - 1) {
								i1 = m_m - 2;
							}
						}
					}
				}
			}
		}
		return E;
	}
}

// ����������������� �������
Matrix Matrix::TransposedMatrix()
{
	Matrix tmp(m_m, m_n);
	for (int i = 0; i < m_n; i++)
	{
		for (int j = 0; j < m_m; j++)
		{
			tmp.m_mat[j][i] = m_mat[i][j];
		}
	}
	return tmp;
}

// �������� �������
Matrix Matrix::InverseMatrix()
{
	Matrix R(m_n, m_m);
	if (m_m != m_n)
	{
		std::cout << "�������� �� ��������������" << std::endl;
		return R;
	}
	else if (m_m > 3)
	{
		std::cout << "�������� �� ��������������" << std::endl;
		return R;
	}

	Matrix M(m_n - 1, m_m - 1);
	Matrix C(m_n, m_m);

	for (int i = 0; i < m_n; i++)
	{
		for (int j = 0; j < m_m; j++)
		{
			C.m_mat[i][j] = m_mat[i][j];
		}
	}

	float d = C.Det();
	std::cout << d << std::endl;

	int cof = 1;
	if (d != 0)
	{
		for (int i = 0; i < m_n; i++)
		{
			for (int j = 0; j < m_m; j++)
			{
				if ((i + j) % 2 != 0)
				{
					cof = (-1);
				}
				R.m_mat[j][i] = C.Minor(i, j).Det() * cof;

				cof = 1;
			}
		}
		for (int i = 0; i < m_n; i++)
		{
			for (int j = 0; j < m_m; j++)
			{
				R.m_mat[i][j] = R.m_mat[i][j] / d;
			}
		}
	}
	else
	{
		std::cout << "�������� ������� �� ���������� (������������� = 0)\n";
		for (int i = 0; i < m_n; i++)
		{
			for (int j = 0; j < m_m; j++)
			{
				R.m_mat[i][j] = m_mat[i][j];
			}
		}
	}
	return R;
}

// ���������� ��������� �����
std::istream& operator>>(std::istream& in, Matrix& mat)
{
	for (int i = 0; i < mat.m_n; i++)
		for (int j = 0; j < mat.m_m; j++)
			in >> mat.m_mat[i][j];
	return in;
}

// ���������� ��������� ������
std::ostream& operator<<(std::ostream& out, const Matrix& mat)
{
	//out << "Matrix " << mat.m_n << "x" << mat.m_m << std::endl;
	for (int i = 0; i < mat.m_n; i++) {
		for (int j = 0; j < mat.m_m; j++)
			out << mat.m_mat[i][j] << " ";
		out << std::endl;
	}
	return out;
}