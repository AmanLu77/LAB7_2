#pragma once
#include <iostream>

class Matrix
{

public:
	// �����������
	Matrix(int n, int m)
	{
		//std::cout << "Constructor" << std::endl;
		m_n = n;
		m_m = m;
		m_mat = new float* [m_n];
		for (int i = 0; i < m_n; i++)
			m_mat[i] = new float[m_m];
	}
	// ����������� �����������
	Matrix(const Matrix& mat)
	{
		m_n = mat.m_n;
		m_m = mat.m_m;

		m_mat = new float* [m_n];
		for (int i = 0; i < m_n; i++)
			m_mat[i] = new float[m_m];

		for (int i = 0; i < m_n; i++)
			for (int j = 0; j < m_m; j++)
				m_mat[i][j] = mat.m_mat[i][j];
	}
	// ������������
	Matrix& operator=(const Matrix& mat)
	{
		m_n = mat.m_n;
		m_m = mat.m_m;

		for (int i = 0; i < m_n; i++)
			for (int j = 0; j < m_m; j++)
				m_mat[i][j] = mat.m_mat[i][j];

		return *this;
	}
	// �������� ��������
	Matrix operator+(const Matrix& mat) 
	{
		//std::cout << "operator+" << std::endl;
		int t = 1, r = 1;
		if (mat.m_m == m_m && mat.m_n == m_n)
		{
			t = mat.m_m;
			r = mat.m_n;
		}
		else
		{
			std::cout << "Error +" << std::endl;
			Matrix tmp1(0, 0);
			return tmp1;
		}
		Matrix tmp(r, t);
		for (int i = 0; i < m_n; i++)
			for (int j = 0; j < m_m; j++)
				tmp.m_mat[i][j] = m_mat[i][j] + mat.m_mat[i][j];
		return tmp;
	}
	
	// �������� ���������
	Matrix operator-(const Matrix& mat) 
	{
		//std::cout << "operator-" << std::endl;
		int t = 1, r = 1;
		if (mat.m_m == m_m && mat.m_n == m_n)
		{
			t = mat.m_m;
			r = mat.m_n;
		}
		else
		{
			std::cout << "Error -" << std::endl;
			Matrix tmp1(0, 0);
			return tmp1;
		}
		Matrix tmp(r, t);
		for (int i = 0; i < m_n; i++)
			for (int j = 0; j < m_m; j++)
				tmp.m_mat[i][j] = m_mat[i][j] - mat.m_mat[i][j];
		return tmp;
	}
	
	// �������� ���������
	Matrix operator*(const Matrix& mat) 
	{
		//std::cout << "operator*" << std::endl;
		int t = 1, r = 1;
		if (mat.m_n == m_m)
		{
			t = m_m;
			r = mat.m_n;
		}
		else
		{
			std::cout << "Error *" << std::endl;
			Matrix tmp1(0, 0);
			return tmp1;
		}

		Matrix tmp(r, t);
		for (int i = 0; i < m_m; i++)
		{
			for (int j = 0; j < mat.m_n; j++)
			{
				tmp.m_mat[i][j] = 0;
				for (int k = 0; k < m_n; k++)
				{
					tmp.m_mat[i][j] += m_mat[i][k] * mat.m_mat[k][j];
				}
			}
		}
		return tmp;
	}

	// ������������ 2�2 � 3�3
	float Det()
	{
		float d = 0;
		if (m_n == 2 && m_m == 2)
		{
			d += m_mat[0][0] * m_mat[1][1];
			d -= m_mat[0][1] * m_mat[1][0];
			return d;
		}
		else if (m_n == 3 && m_m == 3)
		{
			d = m_mat[0][0] * m_mat[1][1] * m_mat[2][2] - m_mat[0][0] * m_mat[1][2] * m_mat[2][1]
				- m_mat[0][1] * m_mat[1][0] * m_mat[2][2] + m_mat[0][1] * m_mat[1][2] * m_mat[2][0]
				+ m_mat[0][2] * m_mat[1][0] * m_mat[2][1] - m_mat[0][2] * m_mat[1][1] * m_mat[2][0];
			return d;
		}
		else if (m_n == 1 && m_m == 1)
		{
			d = m_mat[0][0];
			return d;
		}
		else
		{
			std::cout << "�������� �� ��������������" << std::endl;
			return 0;
		}
	}

	// ����������
	~Matrix()
	{
		//std::cout << "Destructor" << std::endl;
		for (int i = 0; i < m_n; i++)
			delete[] m_mat[i];
		delete m_mat;
	}

	// friend - ���� ������� ������ � private �����/������� ������
	friend std::istream& operator>>(std::istream& os, Matrix& mat);
	friend std::ostream& operator<<(std::ostream& os, const Matrix& mat);

	int Getn() { return m_n; }

	int Getm() { return m_m; }


	// �����
	Matrix Minor(int a1, int a2)
	{
		Matrix E(m_n - 1, m_m - 1);
		if (m_m != m_n)
		{
			std::cout << "�������� �� ��������������" << std::endl;
			return E;
		}
		else
		{
			int i1 = 0, j1 = 0;
			for (int i = 0; i < m_n; i++) {

				for (int j = 0; j < m_m; j++) {

					if (i != a1) {

						if (j != a2) {
							E.m_mat[i1][j1] = m_mat[i][j];
							j1++;

							if (j1 % (m_m - 1) == 0) {

								i1++;
								j1 = 0;

								if (i1 >= m_m - 1) {
									i1 = m_m - 2;
								}
							}
						}
					}
				}
			}
			return E;
		}
	}

	// ����������������� �������
	Matrix TransposedMatrix()
	{
		Matrix tmp(m_m, m_n);
		for (int i = 0; i < m_n; i++)
		{
			for (int j = 0; j < m_m; j++)
			{
				tmp.m_mat[j][i] = m_mat[i][j];
			}
		}
		return tmp;
	}

	// �������� �������
	Matrix InverseMatrix();

	// ������������� ������ ������
private:
	int m_n, m_m;		// ����
	float n;
	float** m_mat;
};

// ���������� ��������� �����
std::istream& operator>>(std::istream& in, Matrix& mat);

// ���������� ��������� ������
std::ostream& operator<<(std::ostream& out, const Matrix& mat);